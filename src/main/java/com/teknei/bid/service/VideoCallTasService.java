package com.teknei.bid.service;

import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.PersonData;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidClieTas;
import com.teknei.bid.persistence.entities.BidScan;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.persistence.repository.BidTasRepository;
import com.teknei.bid.util.tas.TasManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Service
public class VideoCallTasService {

    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidTasRepository bidTasRepository;
    @Value("${tkn.tas.name}")
    private String tasName;
    @Value("${tkn.tas.surname}")
    private String tasSurname;
    @Value("${tkn.tas.lastname}")
    private String tasLastname;
    @Value("${tkn.tas.identification}")
    private String tasIdentification;
    @Value("${tkn.tas.date}")
    private String tasDate;
    @Value("${tkn.tas.scan}")
    private String tasScan;
    @Value("${tkn.tas.id}")
    private String tasOperationId;
    @Value("${tkn.tas.casefile}")
    private String tasCasefile;
    @Value("${tkn.tas.idType}")
    private String tasTypeId;
    @Value("${tkn.tas.recording}")
    private String tasRecording;
    @Value("${tkn.tas.selfie}")
    private String tasSelfie;
    @Autowired
    private BidClieRepository clieRepository;

    @Autowired
    private TasManager tasManager;

    private static final Logger log = LoggerFactory.getLogger(VideoCallTasService.class);


    public void addQuobisSelfieToTAS(byte[] selfie, long id) throws Exception {
        addQuobisData(id, selfie, 1);
    }

    public void addQuobisRecordingToTAS(byte[] caller, long id) throws Exception {
        addQuobisData(id, caller, 2);
    }

    private void addQuobisData(Long id, byte[] data, int type) throws Exception {
        BidScan bidScan = bidScanRepository.findByIdRegi(id);
        BidClieTas bidTas = bidTasRepository.findByIdClie(id);
        Map<String, String> docProperties = getMetadataMapAddress(bidScan.getScanId(), id);
        switch (type) {
            case 1:
                tasManager.addDocument(tasSelfie, bidTas.getIdTas(), null, docProperties, data, "image/png", "Selfie.png");
                break;
            case 2:
                tasManager.addDocument(tasRecording, bidTas.getIdTas(), null, docProperties, data, MediaType.APPLICATION_OCTET_STREAM_VALUE, "GrabacionVC.webm");
                break;
        }
    }

    private Map<String, String> getMetadataMapAddress(String scanId, Long operationId) throws Exception {
        PersonData scanInfo = getPersonalDataFromScan(scanId, operationId);
        Map<String, String> docProperties = new HashMap<>();
        docProperties.put(tasName, scanInfo.getName());
        docProperties.put(tasSurname, scanInfo.getSurename());
        docProperties.put(tasLastname, scanInfo.getSurenameLast());
        docProperties.put(tasIdentification, scanInfo.getPersonalNumber());
        String dateISO8601 = ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT).toString();
        docProperties.put(tasDate, dateISO8601);
        return docProperties;
    }


    /**
     * Obtains the required info correctly parsed
     * @param scanId
     * @return
     * @throws Exception
     */
    public PersonData getPersonalDataFromScan(String scanId, Long operationId) throws Exception {
        PersonData personalData = new PersonData();
        BidClie bidClie = clieRepository.findOne(operationId);
        personalData.setPersonalNumber(String.valueOf(operationId));
        String name = bidClie.getNomClie() == null ? "" : bidClie.getNomClie();
        String surname = bidClie.getApePate() == null ? "" : bidClie.getApePate();
        String surnamelast = bidClie.getApeMate() == null ? "" : bidClie.getApeMate();
        String surnames = surname + " " + surnamelast;
        personalData.setName(name);
        personalData.setSurename(surname);
        personalData.setSurenameLast(surnamelast);
        personalData.setLastNames(surnames);
        return personalData;
    }

    public byte[] findPicture(DocumentPictureRequestDTO requestDTO) {
        return tasManager.getPicture(requestDTO.getId());
    }
}
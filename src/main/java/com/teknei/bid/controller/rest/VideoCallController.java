package com.teknei.bid.controller.rest;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.VideocallRequestDTO;
import com.teknei.bid.service.VideoCallTasService;
import com.teknei.bid.service.remote.QuobisStorageServiceRemote;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/video")
public class VideoCallController {

    @Autowired
    @Qualifier(value = "videocallCommand")
    private Command videocallCommand;
    @Autowired
    private VideoCallTasService videoCallTasService;
    @Autowired
    private QuobisStorageServiceRemote quobisStorageServiceRemote;

    private static final Logger log = LoggerFactory.getLogger(VideoCallController.class);

    @ApiOperation(value = "Adds the resources taken by remote server to the local server")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If all the resources are saved successfully"),
            @ApiResponse(code = 422, message = "If one or more resources could not be saved successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<String> addVideocallData(@RequestBody VideocallRequestDTO videocallRequestDTO) {
        log.info("Requesting with: {}", videocallRequestDTO);
        CommandResponse response = null;
        CommandRequest request = new CommandRequest();
        request.setId(videocallRequestDTO.getIdCustomer());
        request.setUserOpeCrea("tkn-api");
        try {
            response = videocallCommand.execute(request);
            log.info("Response from command: {}", response);
            if (response.getStatus().equals(Status.VIDEOCALL_ERROR)) {
                return new ResponseEntity<>(response.getDesc(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in videocallController for: {} with message: {}", videocallRequestDTO, e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Downloads the picture related to the face of the given customer", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the picture is found successfully"),
            @ApiResponse(code = 422, message = "If the picture could not be found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getImageFromReference(@RequestBody DocumentPictureRequestDTO dto) {
        byte[] image = null;
        try {
            image = videoCallTasService.findPicture(dto);
            if (image == null) {
                return new ResponseEntity<>(image, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(image, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding image for reference: {} with message: {}", dto, e.getMessage());
            return new ResponseEntity<>(image, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Downloads the picture related to the face of the given customer in quobis storage system", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the picture is found successfully"),
            @ApiResponse(code = 404, message = "If the picture could not be found"),
            @ApiResponse(code = 404, message = "If there is a problem with the service"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/downloadQS", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getImageFromQuobisStorage(@RequestBody DocumentPictureRequestDTO dto){
        byte[] image = null;
        try{
            image = quobisStorageServiceRemote.getSelfie(Long.parseLong(dto.getId()));
            return new ResponseEntity<>(image, HttpStatus.OK);
        }catch(FeignException fe){
            if(fe.status() == 404){
                return new ResponseEntity<>(image, HttpStatus.NOT_FOUND);
            }
            log.error("Error getting remote quobis resource: {} with message: {}", dto.getId(), fe.getMessage());
            return new ResponseEntity<>(image, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e){
            log.error("Error getting remote quobis resource: {} with message: {}", dto.getId(), e.getMessage());
            return new ResponseEntity<>(image, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
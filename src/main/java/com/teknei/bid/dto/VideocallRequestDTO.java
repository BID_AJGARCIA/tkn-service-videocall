package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class VideocallRequestDTO implements Serializable {

    private Long idCustomer;
    private String reason;
    private String status;

}